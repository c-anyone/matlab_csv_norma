function [trimmed] = trimmer(all_data,trim_bool)
% TRIMMER Trims input matrix according to bool table
% additionally adjusts the time_vector to start at 0

trimmed=all_data(trim_bool,:);
time_diff=trimmed(1,1);
trimmed(:,1)=trimmed(:,1)-time_diff;

end