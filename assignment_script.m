%% Pungg Fabio Assignment 2 Script
% Read in data from the specified csv file
% process the data, resolve errors, trim time vector
% plot and save the matrix

clear;
clc;

filename='new_step_PUNGG.csv';


%% Import data from the csv
[info_in,param_in,raw_in,info_out,param_out,raw_out] =  import_function(filename);

%% Read parameters
rec_len=str2double(param_in{1});
sample_intervall=str2double(param_in{2});

%% Create time vector
%  rec_len-1 because we start at 0
time_vect=(0:sample_intervall:((rec_len-1)*sample_intervall))';

%% Save the data into 1 Matrix
alldata=[time_vect,raw_in,raw_out];

%% Resolve sensor errors in array, marked by value=666
alldata(:,2:3)=resolve_sensor_errors(alldata(:,2:3));


%% Trim time vector and zero values
alldata=trimmer(alldata,alldata(:,2)>0.5);

%% Read probe attenuation factor from csv
atten_in=str2double(param_in{15});
atten_out=str2double(param_out{15});

%% Apply probe attenuation factor to data array
% chose to divide by it, because multiplying yielded 
% unexpected results
alldata(:,2)=alldata(:,2)./atten_in;
alldata(:,3)=alldata(:,3)./atten_out;

%% Normalize 
alldata(:,3)=alldata(:,3)./alldata(:,2);
alldata(:,2)=alldata(:,2)./alldata(:,2);

%% Read measure date from csv
%  and convert to different format
measure_date=param_in{17}(end-10:end);
measure_date=datestr(datenum(measure_date,'dd.mm.yyyy'),'yyyy_mm_dd');

%% Save table to file
savefile=(['PUNGG_' measure_date '.mat']);
save(savefile,'alldata');


%% plot(alldata(:,1),alldata(:,1),alldata(:,1),alldata(:,3));
createfigure(alldata);

clc
clear all