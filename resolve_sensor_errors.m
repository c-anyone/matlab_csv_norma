function [ resolved ] = resolve_sensor_errors( data )
% RESOLVE_SENSOR_ERRORS Find and resolve any sensor errors
% Sensor errors are marked by value=666
% Set it to the value preceding the error

errors=data==666;
[~, col]=size(data);
while(any(errors))
    index = find(errors);
    if index(1) <= 1
        error('Error resolving sensor errors, values start with error');
    end
    data(errors) = data(index-col);
    errors = data==666;
end
resolved=data;
end

